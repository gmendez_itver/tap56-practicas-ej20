/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;
import practicas.Practica5;

/**
 *
 * @author LaboratorioU005_11
 */
public class EjemploHilos {
    
    int x = 0;
    
    public static void main(String args[]){               
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EjemploHilos().ejecutar();
            }
        });
    }

    private void ejecutar() {
        Hilo h1 = new Hilo("h1",10,1000, this);
        Hilo h2 = new Hilo("h2",15,1500, this);
        Hilo h3 = new Hilo("h3",20,500, this);

        h1.start();
        h2.start();
        h3.start();
        
        for(int i=0; i<30; i++){
            System.out.printf("Proceso principal : i = %d \n",i);
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(EjemploHilos.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }        
        
    }          
}

class Hilo extends Thread {
    int n = 0;
    int t = 0;
    String id = ""; 
    EjemploHilos p;
    
    public Hilo(String _id, int _n, int _t, EjemploHilos _p){
        this.n = _n;
        this.id = _id;        
        this.t = _t;
        this.p = _p;
    }
    
    @Override
    public void run() {        
        for(int i=0; i<this.n; i++){
            System.out.printf("%s : i = %d, %d \n",this.id,i, this.p.x);            
            try {
                Thread.sleep(this.t);
            } catch (InterruptedException ex) {
                Logger.getLogger(EjemploHilos.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }        
    }    
}
