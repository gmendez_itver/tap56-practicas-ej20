/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author macpro1
 */
public class Ejemplo0 implements Runnable {
    
    int x=0;
    Object semaforo = new Object();
    
    public static void main(String args[]) {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ejemplo0().run();
            }
        });
    }    

    @Override
    public void run() {
        
        Hilo h1 = new Hilo("Hilo 1",15000);
        Hilo h2 = new Hilo("Hilo 2",2000);
        Hilo h3 = new Hilo("Hilo 3",2500);
        
        h1.start();
        
        /*
        try {
            h1.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        
        h2.start();
        h3.start();
        
        for(int i=0; i<20; i++){
            System.out.printf("Ejecucion principal, contador: %d, x=%d \n",i,this.x);
            this.x = i;
            
            if(i==10){
                synchronized (semaforo){
                    h1.interrupt();
                    
                }
            }
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Ejemplo0.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    class Hilo extends Thread {
        String id = "";
        int miliseg = 1000;
        public Hilo(String _id, int _miliseg){
            this.id = _id;
            this.miliseg = _miliseg;
        }

        @Override
        public void run() {
            for(int i=0; i<20; i++){
                x = i;
                System.out.printf("Ejecucion de tarea %s, contador: %d, x=%d \n",this.id,i,x);
                try {
                    Thread.sleep(this.miliseg);
                } catch (InterruptedException ex) {
                    System.out.printf("La tarea %s fue despertada de su sueño", this.id);
                    //Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}

