/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author macpro1
 */
public class Ejemplo1 {
    
    public static int x = 999;
    
    public static void main(String args[]){
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ejemplo1().ejecutar();
            }
        });
        
    }
    
    void ejecutar(){
        System.out.println("Ejecutando programa");
        Hilo h1 = new Hilo("H1",10,1000,this);
        Hilo h2 = new Hilo("H2",15,500,this);
        Hilo h3 = new Hilo("H3",20,1500,this);
        
        h1.start();
        
        try {
            h1.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        h2.start();
        h3.start();
        
        for(int c = 0; c<50; c++){
            System.out.printf("Contador principal c = %d\n",c);
        }
    } 
}

class Hilo extends Thread {
    int n = 100;
    String id = "";
    int t;
    Ejemplo1 madre;
    
    public Hilo(String _id,int _n, int _t, Ejemplo1 _madre){
        this.n = _n;
        this.id = _id;
        this.t = _t;
        this.madre = _madre;
    }
    
    @Override
    public void run() {
        for(int i=0; i<n; i++){
            System.out.printf("Hilo %s: Contador i = %d \n", this.id,i);
            System.out.println("x="+madre.x);
            try {
                Thread.sleep(this.t);
            } catch (InterruptedException ex) {
                Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
