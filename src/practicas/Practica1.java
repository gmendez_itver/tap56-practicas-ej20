/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author LaboratorioU005_11
 */
public class Practica1 extends JFrame implements ActionListener {
    JTextField tf;
            
    Practica1(){
        this.setTitle("Practica 1");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(300,150);
        
        this.setLayout(new FlowLayout());
        
        JLabel lbl = new JLabel("Escribe un nombre");
        tf = new JTextField(20);   
        JButton btn = new JButton("Saludar!");
        
        btn.addActionListener(this);
        
        this.add(lbl);
        this.add(tf);
        this.add(btn);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Practica1().setVisible(true);
            }
        }); 
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        //To change body of generated methods, choose Tools | Templates.
        JOptionPane.showMessageDialog(this, "Hola "+tf.getText());
    }
}
